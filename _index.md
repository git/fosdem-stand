---
description: "The LibreSOC Project is an ethically developed processor, based on OpenPOWER,\
  \ intended for use in smartphones netbooks tablets chromebooks and Industrial Embedded\
  \ SBC products.  There will be no DRM, no TPM, and no backdoor spying co-processors.\r\
  \n\r\nThe entire source code, traditionally known to OEMs as a \"Board Support Package\"\
  , will not only be available free of charge under Libre Licenses, right to the BIOS\
  \ level, the hardware HDL will also likewise be available (and is already available\
  \ as it is developed).  Access to a full BSP with greatly simplified drivers for\
  \ 3D and Video is a huge cost and time saving for businesses.\r\n\r\nIt's also a\
  \ fantastic educational and research base.  Which would you rather have: a Pi-style\
  \ SBC where the GPU boots the processor from a closed source binary and kids are\
  \ taught that to play videos they must pay a license fee for a closed binary decoder,\
  \ or would you prefer that they are able to get the full source of even the hardware\
  \ and boot the entire processor including the GPU and VPU on a $200 FPGA and learn\
  \ how it works?"
layout: stand
logo: stands/the_libresoc_project__a_hybrid_3d_cpu_vpu_gpu/logo.png
new_this_year: LibreSOC has EUR 450,000 funding from NLnet,
  for a Video driver, for a MESA 3D driver, a Power ISA Simulator,
  and many more.  We are also funded by NGI POINTER to make a
  Gigabit Ethernet Hub processor (running OpenWRT and LibreCMC).
  Ridiculous amount going on: you're more than welcome to help.
showcase: To learn that there exists an alternative to proprietary processors
  from Intel, ARM and AMD, and that yes, they can actually be part of making
  that happen.  NLnet donations are available, and we have profit-sharing
  in any commercial ventures and spin-offs.  With thanks to Staf Verhagen
  of Chips4Makers - https://chips4makers.io/ - for the 100% Libre FlexLib
  Cell Library including SRAM; Jean-Paul and the team from https://lip6.fr
  for coriolis2, and NLnet https://nlnet.nl for funding our research.
themes:
- Multimedia and graphics
title: 'The LibreSOC Project'
website: http://libre-soc.org
show_on_overview: true
chatroom: libresoc
---
